Roadmap
=============

- [] File-based Routing System
- [] Twig Rendering
- [] Global & Route Middleware
- [] Robust Routing System
- [] Routing Events (Start, Process, End, Fallback)
- [] Basic Response Handling
    - [] view
    - [] json
    - [] xml
- [] Plugins
- [] Request Validators
- [] Exception Handling
- [] Manual Auto API Adapters and Reponses for REST Routes
- [] API Adapters and Responses for REST Routes (manual creation & registration)
- [] Auto registering and generation of API Adapters and Responses for REST Routes
- [] CLI for creating various classes (Models, Validators, etc.)
- [] Global Helper Functions

See the [open issues](https://git.hou.snaju.com/snaju-oss/inception-framework/-/issues) for the most up to date list of propsed features (and known issues)