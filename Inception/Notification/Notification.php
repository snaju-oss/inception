<?php

namespace Snaju\Inception\Notification;

interface Notification
{
    /**
     * @param $object
     * @return Notification
     * Turn input data into structured notification data within this class.
     */
    public function encode($object): Notification;

    /**
     * @param Notification $notification
     * @return mixed
     * Return a required notification data from this notification object.
     */
    public function decode();
}