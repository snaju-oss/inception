<?php

namespace Snaju\Inception\Notification;

class NotificationManager
{
    private static array $adapters = [];

    public static function registerAdapter(NotificationAdapter $adapter)
    {

        if (!isset(self::$adapters[$adapter->getType()])) {
            self::$adapters[$adapter->getType()] = [];
        }

        self::$adapters[$adapter->getType()][get_class($adapter)] = $adapter;
    }

    public static function getAdapter(string $class)
    {
        return self::$adapters[$class];
    }

    public static function send(string $class, Notification $notification)
    {
        if (isset(self::$adapters[$class])) {
            $classes = self::$adapters[$class];

            foreach ($classes as $adapterName => $obj) {
                if ($obj instanceof NotificationAdapter) {
                    $obj->send($notification);
                }
            }

        }
    }

}