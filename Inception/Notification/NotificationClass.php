<?php

namespace Snaju\Inception\Notification;

class NotificationClass
{
    const EMAIL = 'email';
    const SMS = 'sms';
    const VOIP = "voip";
    const WEB = 'web';
    const THIRD_PARTY = '3rd_party';

}