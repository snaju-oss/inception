<?php

namespace Snaju\Inception\Notification;

interface NotificationAdapter
{

    function getType(): string;

    function send(Notification $notification): bool;

}
