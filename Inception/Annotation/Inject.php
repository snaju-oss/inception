<?php

namespace Snaju\Inception\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Inject extends InceptionAnnotation
{
}