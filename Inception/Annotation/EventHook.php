<?php

namespace Snaju\Inception\Annotation;

use Snaju\Inception\Event\HookPriority;

/**
 * @Annotation
 */
class EventHook extends InceptionAnnotation
{
    public int $priorirty = HookPriority::NORMAL;
}