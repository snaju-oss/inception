<?php

namespace Snaju\Inception\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target("CLASS")
 */
class ApiModel extends InceptionAnnotation
{
    public $path;
    public $methods = ["list", "get", "create", "delete", "patch"];
}