<?php

namespace Snaju\Inception\Annotation;

/**
 * @Annotation
 */
class InceptionRoute extends InceptionAnnotation
{
    /**
     * @var string
     * @Required
     */
    public $attachToPath;
}
