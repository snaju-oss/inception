<?php

namespace Snaju\Inception\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target("CLASS")
 */
class ApiRequireAuth extends InceptionAnnotation
{
}