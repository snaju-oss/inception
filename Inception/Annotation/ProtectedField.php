<?php

namespace Snaju\Inception\Annotation;

/**
 * @Annotation
 */
class ProtectedField extends InceptionAnnotation
{

    /**
     * @var boolean
     */
    public bool $isProtected = true;

    public bool $readable = true;

    public bool $writable = false;

}