<?php

namespace Snaju\Inception\Annotation;

class AnnotationManager
{

    public static function init()
    {
        ApiModel::init();
        ApiRequireAuth::init();
        ApiRequireScope::init();
        EventHook::init();
        InceptionRoute::init();
        Inject::init();
        LazyLoader::init();
        Load::init();
        OVERRIDE::init();
        ProtectedField::init();
        Singleton::init();
    }

}