<?php

namespace Snaju\Inception\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target("CLASS")
 */
class LazyLoader extends InceptionAnnotation
{

    public ?string $loadMetaToKey = null;

}