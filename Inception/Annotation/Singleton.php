<?php

namespace Snaju\Inception\Annotation;

/**
 * @Annotation
 */
class Singleton extends InceptionAnnotation
{
    // Do Nothing we inject
}