<?php

namespace Snaju\Inception\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target("CLASS")
 */
class ApiRequireScope extends InceptionAnnotation
{
    public string $scope;
    public $methods = ["list", "get", "create", "delete", "patch"];
}