<?php

namespace Snaju\Inception\Event;

class EventHookObject
{

    private int $priority;

    private $runnable;

    /**
     * EventHook constructor.
     * @param int $priority
     * @param $runnable
     */
    public function __construct(int $priority, callable $runnable)
    {
        $this->priority = $priority;
        $this->runnable = $runnable;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getRunnable()
    {
        return $this->runnable;
    }

    /**
     * @param mixed $runnable
     */
    public function setRunnable($runnable): void
    {
        $this->runnable = $runnable;
    }

}