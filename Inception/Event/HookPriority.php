<?php

namespace Snaju\Inception\Event;

interface HookPriority
{
    const FIRST = 999;
    const HIGH = 1;
    const NORMAL = 0;
    const LOW = -1;
    const LAST = -999;
}