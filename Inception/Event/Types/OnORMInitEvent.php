<?php

namespace Snaju\Inception\Event\Types;

use Snaju\Inception\Event\Event;
use Snaju\Inception\Exception\Exception;
use Snaju\Inception\InceptionCore;
use Snaju\Inception\Route\Route;
use Snaju\Inception\Route\RouteFile;
use Snaju\Inception\Route\RouteManager;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * The OnRouteStart event is responsible for finding a matched route and
 * calling any RouteFile(s) associated with the route/path.
 *
 * @package Snaju\Inception\Event\Types
 */
class OnORMInitEvent extends Event
{

    private AbstractAdapter $cachePool;

    public function getCachePool(): AbstractAdapter
    {
        return $this->cachePool;
    }

    public function setCachePool(AbstractAdapter $cachePool): void
    {
        $this->cachePool = $cachePool;
    }

    public function run(Event $event): Event
    {
        return $this;
    }

    public function onEnd(Event $event)
    {

        $cachePool = new FilesystemAdapter('', 0, InceptionCore::getBase() . "/tmp/orm");
        $this->cachePool = $cachePool;

        return $event;
    }


}