<?php

namespace Snaju\Inception\Event\Types;

use Snaju\Inception\Event\Event;
use Snaju\Inception\Http\Response\ResponseHandler;

// TODO: add docs
class OnRouteEnd extends Event
{

    private string $path;

    /**
     * OnRouteEvent constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function run(Event $event): Event
    {
        return $this;
    }

    public function onEnd(Event $event): Event
    {
        // Send the response to the client
        ResponseHandler::getInstance()->make()->send();

        return $event;
    }

    /**
     * @return mixed|string
     */
    public function getPath(): string
    {
        return $this->path;
    }

}