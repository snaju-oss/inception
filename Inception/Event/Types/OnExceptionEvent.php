<?php

namespace Snaju\Inception\Event\Types;

use Snaju\Inception\Event\Event;

// TODO: add docs
class OnExceptionEvent extends Event
{

    private \Throwable $exception;

    /**
     * OnExceptionEvent constructor.
     * @param \Throwable $exception
     */
    public function __construct(\Throwable $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return \Throwable
     */
    public function getException(): \Throwable
    {
        return $this->exception;
    }


    public function run(Event $event): Event
    {
        return $event;
    }

    public function onEnd(Event $event)
    {
        $e = $event->getException();
        echo "<span style='color: red;'><h1>Fatal Error {</h1><hr/>" .
            "<p>" . $e->getMessage() . "</p>" .
            "<p><i>" . $e->getFile() . ":" . $e->getLine() . " </i></p></span><hr/><pre>{$e->getTraceAsString()}</pre>";
        exit;
    }

}