<?php

namespace Snaju\Inception\Event\Types;

use Snaju\Inception\Event\Event;

// TODO: add docs
class OnRouteFallback extends Event
{

    private string $path;

    /**
     * OnRouteEvent constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function run(Event $event): Event
    {
        return $this;
    }

    public function onEnd(Event $event): Event
    {
        // echo 'EVENT::: Route Fallback called. <br />';

        return $event;
    }

    /**
     * @return mixed|string
     */
    public function getPath(): string
    {
        return $this->path;
    }

}