<?php

namespace Snaju\Inception\Event\Types;

use Snaju\Inception\Event\Event;

class OnPreAppReadyEvent extends Event
{
    public function run(Event $event): Event
    {
        return $event;
    }

}