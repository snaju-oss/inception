<?php

namespace Snaju\Inception\Event\Types;

use Snaju\Inception\Event\Event;
use Snaju\Inception\Exception\Exception;
use Snaju\Inception\InceptionCore;
use Snaju\Inception\Request\WebRequest;
use Snaju\Inception\Route\Loaders\Twig\TemplateManager;
use Snaju\Inception\Route\Route;
use Snaju\Inception\Route\RouteManager;
use Snaju\Inception\Route\RouteMiddleware;
use Snaju\Inception\Route\RouteNode;

// TODO: add docs
class OnNoRouteFoundEvent extends Event
{

    private Exception $e;

    /**
     * OnNoRouteFoundEvent constructor.
     * @param Exception $e
     */
    public function __construct(Exception $e)
    {
        $this->e = $e;
    }


    public function run(Event $event): Event
    {
        return $this;
    }

    /**
     * @return Exception
     */
    public function getE(): Exception
    {
        return $this->e;
    }

}