<?php

namespace Snaju\Inception\Event\Types;

use Doctrine\ORM\Configuration;
use Snaju\Inception\Event\Event;
use Snaju\Inception\Exception\Exception;
use Snaju\Inception\InceptionCore;
use Snaju\Inception\Route\Route;
use Snaju\Inception\Route\RouteFile;
use Snaju\Inception\Route\RouteManager;
use Symfony\Component\Cache\Adapter\AbstractAdapter;

/**
 * The OnRouteStart event is responsible for finding a matched route and
 * calling any RouteFile(s) associated with the route/path.
 *
 * @package Snaju\Inception\Event\Types
 */
class OnORMConfigEvent extends Event
{

    private Configuration $config;

    /**
     * @param Configuration $config
     */
    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    public function getConfig(): Configuration
    {
        return $this->config;
    }

    public function setConfig(Configuration $config): void
    {
        $this->config = $config;
    }



    public function run(Event $event): Event
    {
        return $this;
    }
}