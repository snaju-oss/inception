<?php

namespace Snaju\Inception\Event\Types;

use Snaju\Inception\Event\Event;

// TODO: add docs
class OnRouteProcess extends Event
{

    private string $path;

    /**
     * OnRouteEvent constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function run(Event $event): Event
    {
        return $this;
    }

    public function onEnd(Event $event): Event
    {
//        echo '<br />';
//        echo 'EVENT::: OnRouteProcess <br />';
//        echo '<br />';


















        return $event;
    }

    /**
     * @return mixed|string
     */
    public function getPath(): string
    {
        return $this->path;
    }

}