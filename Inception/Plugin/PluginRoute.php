<?php

use Snaju\Inception\InceptionCore;

class PluginRoute extends \Snaju\Inception\Route\Route
{
    private \Snaju\Inception\Plugin\PluginObject $plugin;
    private string $routeFile;

    /**
     * PluginRoute constructor.
     * @param string $routeFile
     */
    public function __construct($url, $file, \Snaju\Inception\Plugin\PluginObject $pluginObject)
    {
        $this->plugin = $pluginObject;
        $this->routeFile = $file;
        parent::__construct($url);
    }


    public function onCall(\Snaju\Inception\Http\Request\WebRequest $request)
    {
        // Call the route.
        require $this->routeFile;
    }
}

?>