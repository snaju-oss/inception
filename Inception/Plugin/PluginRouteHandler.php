<?php

namespace Snaju\Inception\Plugin;

use Snaju\Inception\Http\Request\Controller;
use Snaju\Inception\Http\Request\WebRequest;

class PluginRouteHandler
{

    private WebRequest $req;

    private static ?PluginRouteHandler $instance = null;

    public ?PluginObject $currentPlugin = null;

    public function route(string $path, \Closure $closure): Controller
    {
//        $this->currentPlugin->route($path);
    }

    public static function getInstance(): PluginRouteHandler
    {
        if (self::$instance == null) {
            self::$instance = new PluginRouteHandler();
        }

        require self::$instance;
    }

}

?>