<?php

namespace Snaju\Inception\Plugin;

use Snaju\Inception\Route\RouteManager;

class PluginObject
{

    private PluginInterface $plugin;

    private string $dir;

    private array $depends = [];

    private object|null $composer = null;

    private PluginConfig|null $config = null;

    private bool $autoload = true;

    /**
     * PluginObject constructor.
     * @param $plugin
     * @param $dir
     */
    public function __construct(PluginInterface $plugin, string $dir)
    {
        $this->plugin = $plugin;
        $this->dir = $dir;
    }

    /**
     * @return bool
     */
    public function isAutoload(): bool
    {
        return $this->autoload;
    }

    /**
     * @param bool $autoload
     */
    public function setAutoload(bool $autoload): void
    {
        $this->autoload = $autoload;
    }

    /**
     * @return PluginConfig|null
     */
    public function getConfig(): ?PluginConfig
    {
        return $this->config;
    }

    /**
     * @param PluginConfig|null $config
     */
    public function setConfig(?PluginConfig $config): void
    {
        $this->config = $config;
    }


    /**
     * @return array
     */
    public function getDepends(): array
    {
        return $this->depends;
    }

    /**
     * @param array $depends
     */
    public function setDepends(array $depends): void
    {
        $this->depends = $depends;
    }

    /**
     * @return array
     */
    public function getComposer(): array
    {
        return $this->composer;
    }

    /**
     * @param array $composer
     */
    public function setComposer(object $composer): void
    {
        $this->composer = $composer;
    }


    /**
     * @return PluginInterface
     */
    public function &getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }

}