<?php

namespace Snaju\Inception\Plugin;

interface PluginInterface
{

    public function onEnable();

    public function onDisable();

    public function getVersion(): float;

    public function getSimpleName(): string;
    
}