<?php

namespace Snaju\Inception\Console;

interface ConsoleInterface
{
    
//    public function __construct($baseDir = '');

    public function handle(ConsoleInput $input, ConsoleOutput $output);

    public function output();

    public function terminate($input, $status);

}