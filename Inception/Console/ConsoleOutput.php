<?php

namespace Snaju\Inception\Console;

use Snaju\Inception\Exception\InvalidArgumentException;

class ConsoleOutput
{
    
    protected $fgColors = [
        'black'         => '0;30',
        'dark-grey'     => '1;30',
        'red'           => '0;31',
        'light-red'     => '1;31',
        'green'         => '0;32',
        'light-green'   => '1;32',
        'brown'         => '0;33',
        'yellow'        => '1;33',
        'blue'          => '0;34',
        'light-blue'    => '1;34',
        'magenta'       => '0;35',
        'light-magenta' => '1;35',
        'cyan'          => '0;36',
        'light-cyan'    => '1;36',
        'light-grey'    => '0;37',
        'white'         => '1;37',
    ];
    
    protected $bgColors = [
        'black'         => '40',
        'red'           => '41',
        'green'         => '42',
        'yellow'        => '43',
        'blue'          => '44',
        'magenta'       => '45',
        'cyan'          => '46',
        'light-grey'    => '47',
    ];
    
    public function printOutput($text, bool $endWithNewLine = false, ?string $textColor = null, ?string $bgColor = null): void
    {
        if (!empty($textColor) && !array_key_exists($textColor, $this->fgColors)) {
            throw new InvalidArgumentException("$textColor is not an accepted color for textColor.");
        }

        if (!empty($bgColor) && !array_key_exists($bgColor, $this->bgColors)) {
            throw new InvalidArgumentException("$bgColor is not an accepted color for bgColor.");
        }
        
        $modifiedText = "\e[";
        
        if (!empty($textColor)) {
            $modifiedText .= $this->fgColors[$textColor];
        }
        
        if (!empty($bgColor)) {
            $modifiedText .= ';' . $this->bgColors[$bgColor];
        }
        
        $modifiedText .= "m$text\e[0;0m";
        
        if ($endWithNewLine) {
            $modifiedText .= "\n";
        }
        
        echo $modifiedText;
    }

    public function error($message): void
    {
        $this->printOutput($message, true, 'red');
    }
    
    public function warning($message): void
    {
        $this->printOutput($message, true, 'yellow');
    }
    
    public function success($message): void
    {
        $this->printOutput($message, true, 'green');
    }
    
    public function line(string $message, ?string $textColor = null, ?string $bgColor = null): void
    {
        $this->printOutput("$message", true, $textColor, $bgColor);
    }
    
    public function tabbedLine(string $message, ?string $textColor = null, ? string $bgColor = null): void
    {
        $this->printOutput("\t$message", true, $textColor, $bgColor);
    }

    /**
     * @todo implement table method on ConsoleOutput
     */
    public function table(): void
    {
        
    }

    /**
     * @todo implement commands method on ConsoleOutput
     */
    public function commands(): void
    {
        
    }

    public function asciiBranding(?string $version = null): void
    {
        echo "\n   _____             _         _____                      _   _             
  / ____|           (_)       |_   _|                    | | (_)            
 | (___  _ __   __ _ _ _   _    | |  _ __   ___ ___ _ __ | |_ _  ___  _ __  
  \___ \| '_ \ / _` | | | | |   | | | '_ \ / __/ _ | '_ \| __| |/ _ \| '_ \ 
  ____) | | | | (_| | | |_| |  _| |_| | | | (_|  __| |_) | |_| | (_) | | | |
 |_____/|_| |_|\__,_| |\__,_| |_____|_| |_|\___\___| .__/ \__|_|\___/|_| |_|
                   _/ |                            | |                      
                  |__/                             |_|                      ";
        echo "\n============================================================================\n";
        
        if (! empty($version)) {
            $this->printOutput("*******************************", false);
            $this->printOutput(" Version ${version} ", false, 'green');
            $this->printOutput("******************************", true);
        }
        
        echo "============================================================================\n";
    }

    public function newline(): void
    {
        echo "\n";
    }

}