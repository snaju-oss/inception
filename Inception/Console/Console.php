<?php

namespace Snaju\Inception\Console;

class Console implements ConsoleInterface
{
    
    protected $baseDir = __DIR__;

    public function __construct($baseDir)
    {
        $this->baseDir = $baseDir;
    }

    public function handle(ConsoleInput $input, ConsoleOutput $output)
    {

    }

    public function output()
    {

    }

    public function terminate($input, $status)
    {
        exit;
        // release any used resources
    }

}