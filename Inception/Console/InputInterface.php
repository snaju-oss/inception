<?php

namespace Snaju\Inception\Console;

interface InputInterface
{

    public function parse();
    
    public function getArgs(): array;
    
    public function getNamespace(): string;
    
    public function getCommand(): string;
    
    public function getSubcommand(): string;
    
    public function getFullCommand(): string;
    
    public function getResourceResultName(): string;
    
    public function getOptions(): object;
    
    public function hasDefaultNamespace(): bool;
    
    public function hasSubcommand(): bool;
    
    public function hasOptions(): bool;

}