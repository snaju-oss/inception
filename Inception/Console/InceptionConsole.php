<?php

namespace Snaju\Inception\Console;

use MongoDB\Driver\Command;

class InceptionConsole extends Console
{

    protected $registeredCommands  = [
        'help'                      => \Snaju\Inception\Console\Command\HelpCommand::class,
        'init'                      => \Snaju\Inception\Console\Command\InitCommand::class,
        'install:plugin'            => \Snaju\Inception\Console\Command\InstallPluginCommand::class,
        'make:config'               => \Snaju\Inception\Console\Command\MakeConfigCommand::class,
        'make:middleware'           => \Snaju\Inception\Console\Command\MakeMiddlewareCommand::class,
        'make:model'                => \Snaju\Inception\Console\Command\MakeModelCommand::class,
        'make:route'                => \Snaju\Inception\Console\Command\MakeRouteCommand::class,
        'make:view'                 => \Snaju\Inception\Console\Command\MakeViewCommand::class,
    ];

    /**
     * Retrieves the app info from the global app object.
     * @return object
     */
    protected function getAppInfo(): object
    {
//        return app();
        return (object)[];
    }
    
    public function handle(ConsoleInput $input, ConsoleOutput $output): int
    {

        // check cached commands
        // check default commands
            // check console provider commands
        
        // check if empty command(s) and throw an error here
        
        
        var_dump($input);
                
        $path = $input->getFullCommand();
        
        
        
        
        exit;
        
        if (array_key_exists($path, $this->registeredCommands)) {
            $namespace = $this->registeredCommands[$path]; 
            $command = new $namespace;
            $command->setBaseDir($this->baseDir);
            $command->process($input, $output);
        }
            else {
                echo 'no command found';
            }
        
        
        
        
        
        return 0;

    }

    public function output()
    {

    }

    public function terminate($input, $status)
    {
//        exit;
        // release any used resources
    }

}