<?php

namespace Snaju\Inception\Console;

use Snaju\Inception\Exception\Exception;
use Snaju\Inception\Exception\InvalidConfigException;

class InceptionCommand
{
    private object $appInfo;
    
    private string $baseDir;
    
    protected string $name = '';

    protected string $description = '';

    protected array $options = [];

    protected array $defaultOptions = [];

    protected function getPath($path): string
    {
//        $path = str_replace('/', "/", $path);
        return $this->baseDir . "/" . $path;
    }

    public function __construct()
    {
        $this->appInfo = new \stdClass();
    }

    /**
     * @return object|\stdClass
     */
    public function getAppInfo()
    {
        return $this->appInfo;
    }

    /**
     * @param object|\stdClass $appInfo
     */
    public function setAppInfo($appInfo): void
    {
        $this->appInfo = $appInfo;
    }

    /**
     * @return string
     */
    public function getBaseDir(): string
    {
        return $this->baseDir;
    }

    /**
     * @param string $baseDir
     */
    public function setBaseDir(string $baseDir): void
    {
        $this->baseDir = $baseDir;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function getDefaultOptions(): array
    {
        return $this->defaultOptions;
    }

    /**
     * @param array $defaultOptions
     */
    public function setDefaultOptions(array $defaultOptions): void
    {
        $this->defaultOptions = $defaultOptions;
    }
    
    
    
    

    
    
    public function getAppVersion(): ?string
    {
        $version = null;
        
        if (file_exists($this->getPath('composer.json'))) {
            $composerConfig = json_decode(file_get_contents($this->getPath('composer.json'), true));
            
            if ($composerConfig->version) {
                $version = $composerConfig->version;
            }
        }
        
        return $version;
    }
    
}