<?php

namespace Snaju\Inception\Console\Command;

use Snaju\Inception\Console\InceptionCommand;

class MakeRouteCommand extends InceptionCommand
{

    public $name = 'make:route';
    
    public $description = 'creates route along with a controller that responds to specific request methods';
    
    public $options = [
        '--all' => 'creates route with all controller request methods.',
        '--only' => 'creates route with only the specified controller request methods.',
        '--except' => 'creates route with all controller request methods except those specified.',
    ];
    
    public $defaultOptions = [
        '--only' => 'get,post,patch,delete',
    ];

    
    

}