<?php

namespace Snaju\Inception\Http\Request\Validators\Rules;

use Snaju\Inception\Http\Request\Validators\ValidatorRule;

/**
 * Validates that the value provided for the specified field is less than the rule's value.
 *
 * @package Snaju\Inception\Http\Request\Validators\Rules
 */
class LessThan extends ValidatorRule
{

    public function isValid($dataArray): bool
    {
        $valid = $this->optional;
        $bodyData = $dataArray['body'];

        if (array_key_exists($this->field, $bodyData)) {
            if ($bodyData[$this->field] < $this->ruleValue) {
                $valid = true;
            }
                else {
                    $valid = false;
                }
        }

        return $valid;
    }
    
    public function getErrorMessage(): string
    {
        return "The `{$this->field}` must be less than $this->ruleValue.";
    }

}