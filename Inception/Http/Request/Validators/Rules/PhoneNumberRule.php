<?php

namespace Snaju\Inception\Http\Request\Validators\Rules;

use Snaju\Inception\Http\Request\Validators\ValidatorRule;
use Snaju\Inception\Util\Util;

/**
 * Validates that the value provided for the specified field is a string.
 *
 * @package Snaju\Inception\Http\Request\Validators\Rules
 */
class PhoneNumberRule extends ValidatorRule
{

    public function isValid($dataArray): bool
    {
        $valid = $this->optional;
        $bodyData = $dataArray['body'];

        if (array_key_exists($this->field, $bodyData)) {
            $valid = Util::isPhoneNumber($bodyData[$this->field]);
        }

        return $valid;
    }

    public function getErrorMessage(): string
    {
        return "The {$this->field} must be a string.";
    }

}