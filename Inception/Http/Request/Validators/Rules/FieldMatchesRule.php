<?php

namespace Snaju\Inception\Http\Request\Validators\Rules;

use Snaju\Inception\Http\Request\Validators\ValidatorRule;

/**
 * Validates that the value provided for the specified field is a string.
 *
 * @package Snaju\Inception\Http\Request\Validators\Rules
 */
class FieldMatchesRule extends ValidatorRule
{

    public function isValid($dataArray): bool
    {
        $valid = $this->optional;
        $bodyData = $dataArray['body'];

        if (array_key_exists($this->field, $bodyData)) {
            if ($bodyData[$this->field] == $bodyData[explode(";", $this->ruleValue)[0]]) {
                $valid = true;
            }
        }

        return $valid;
    }

    public function getErrorMessage(): string
    {
        return explode(",", $this->ruleValue)[1];
    }

}