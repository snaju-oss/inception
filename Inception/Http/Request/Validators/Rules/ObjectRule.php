<?php

namespace Snaju\Inception\Http\Request\Validators\Rules;

use Snaju\Inception\Http\Request\Validators\ValidatorRule;

/**
 * Validates that the value provided for the specified field is an object.
 *
 * @package Snaju\Inception\Http\Request\Validators\Rules
 */
class ObjectRule extends ValidatorRule
{

    public function isValid($dataArray): bool
    {
        $valid = $this->optional;
        $bodyData = $dataArray['body'];

        // TODO: write valid ObjectRule; right now it's empty

//        if (array_key_exists($this->field, $bodyData)) {
//
//            echo gettype($bodyData[$this->field]);
//
//            if (is_object($bodyData[$this->field])) {
//                $valid = true;
//            }
//                else {
//                    $valid = false;
//                }
//        }

        return $valid;
    }
    
    public function getErrorMessage(): string
    {
        return "The `{$this->field}` must be an object.";
    }

}