<?php

namespace Snaju\Inception\Http\Request\Validators\Rules;

use Snaju\Inception\Http\Request\Validators\ValidatorRule;

/**
 * Validates that a value has been provided for the specified field. This value may be null or empty.
 *
 * @package Snaju\Inception\Http\Request\Validators\Rules
 */
class PresentRule extends ValidatorRule
{

    public function isValid($dataArray): bool
    {
        $valid = false;
        $bodyData = $dataArray['body'];

        if (array_key_exists($this->field, $bodyData)) {
            $valid = true;
        }

        return $valid;
    }
    
    public function getErrorMessage(): string
    {
        return "`{$this->field}` must be present on the request (can be null).";
    }

    public function stopOnError(): bool
    {
        return true;
    }

}