<?php

namespace Snaju\Inception\Http\Request\Validators\Rules;

use Snaju\Inception\Http\Request\Validators\ValidatorRule;

/**
 * Class NumericRule
 *
 * Validates that the value provided for the specified field is numeric.
 *
 * @package Snaju\Inception\Http\Request\Validators\Rules
 */
class NumericRule extends ValidatorRule
{

    public function isValid($dataArray): bool
    {
        $valid = $this->optional;
        $bodyData = $dataArray['body'];

        if (array_key_exists($this->field, $bodyData)) {
            if (is_numeric($bodyData[$this->field])) {
                $valid = true;
            }
                else {
                    $valid = false;
                }
        }

        return $valid;
    }
    
    public function getErrorMessage(): string
    {
        return "The `{$this->field}` must be a numeric value.";
    }

}