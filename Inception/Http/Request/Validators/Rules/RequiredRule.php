<?php

namespace Snaju\Inception\Http\Request\Validators\Rules;

use Snaju\Inception\Http\Request\Validators\ValidatorRule;

/**
 * Validates that a value has been provided for the specified field and is not empty or null.
 *
 * @package Snaju\Inception\Http\Request\Validators\Rules
 */
class RequiredRule extends ValidatorRule
{

    public function isValid($dataArray): bool
    {
        $valid = false;
        $bodyData = $dataArray['body'];

        if (array_key_exists($this->field, $bodyData)) {
            if (! empty($bodyData[$this->field])) {
                $valid = true;
            }
        }

        return $valid;
    }
    
    public function getErrorMessage(): string
    {
        return "The `{$this->field}` is a required field. It cannot be empty or null";
    }

    public function stopOnError(): bool
    {
        return true;
    }

}