<?php

namespace Snaju\Inception\Http\Request\Validators;

class RuleMetaData
{

    private $rule;

    private $value;

    private $errorOverride = null;

    /**
     * RuleMetaData constructor.
     * @param $rule
     * @param $value
     * @param null $errorOverride
     */
    public function __construct($rule, $value, $errorOverride = null)
    {
        $this->rule = $rule;
        $this->value = $value;
        $this->errorOverride = $errorOverride;
    }

    /**
     * @return mixed
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param mixed $rule
     */
    public function setRule($rule): void
    {
        $this->rule = $rule;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return null
     */
    public function getErrorOverride()
    {
        return $this->errorOverride;
    }

    /**
     * @param null $errorOverride
     */
    public function setErrorOverride($errorOverride): void
    {
        $this->errorOverride = $errorOverride;
    }
}

?>