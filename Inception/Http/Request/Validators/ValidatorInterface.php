<?php

namespace Snaju\Inception\Http\Request\Validators;

interface ValidatorInterface
{
    /**
     * Adds a ValidatorRule to use with the created Validator.
     *
     * @param string $identifier The "map name" or name to reference this rule by.
     * @param string $rule The ValidatorRule's namespace (FQPN).
     * @return mixed
     */
    public function addRule(string $identifier, string $rule);

    /**
     * Validates a field's value using the provided ValidatorRule's map name.
     *
     * @param array $data
     * @param bool $sendErrorResponses
     * @return mixed
     */
    public function validate(array $data, bool $sendErrorResponses);

    /**
     * Returns all the errors in a multidimensional array with each error nested under the data field's name.
     *
     * @return array
     */
    public function getErrors(): array;
}