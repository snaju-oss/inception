<?php

namespace Snaju\Inception\Http\Middleware;

use Snaju\Inception\Http\Request\WebRequest;

interface MiddlewareInterface
{

    public function process(WebRequest $request, \Closure $next);

}