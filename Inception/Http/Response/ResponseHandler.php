<?php

namespace Snaju\Inception\Http\Response;

use Snaju\Inception\Http\Response\Enums\ResponseFormat;
use Snaju\Inception\Route\Loaders\Twig\TemplateManager;

class ResponseHandler
{

    private static ?ResponseHandler $instance = null;

    protected Response $response;

    private bool $useControllerOutput = true;

    public static function getInstance(): ResponseHandler
    {
        if (self::$instance === null) {
            self::$instance = new ResponseHandler();
        }
        return self::$instance;
    }

    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * @return bool
     */
    public function isUseDefaultResponse(): bool
    {
        return $this->useDefaultResponse;
    }

    /**
     * Creates a new Response object.
     * @return ResponseHandler
     */
    public function &makeNew(): ResponseHandler
    {
        $this->response = new Response();
        return $this;
    }

    /**
     * Retrieves the raw Response object. When this is used, headers, cookies, etc.,
     * will need to be attached manually.
     * @return Response
     */
    public function &make(): Response
    {
        return $this->response;
    }

//    public function &headers(array $headers = []): ResponseHandler
//    {
//
//
//
//
//        return $this;
//    }
//
//    public function &cookies(): ResponseHandler
//    {
//
//
//
//        return $this;
//    }

    public function &render(string $view, array $data = [], $status = 200, $headers = []): string
    {
        $templateId = $view === 'index' ? '/' : $view;
        $renderer = TemplateManager::render($templateId, $data);
        http_response_code($status);


        // TODO: reconsider adding headers to the render method

        echo $renderer;
        exit;
    }

    public function &void($status = 200) {
        http_response_code($status);

        // Do Nothing, this will use the normal logic for a tempalte/ view

        $this->useControllerOutput = false;
        return $this;
    }

//    public function &raw(): ResponseHandler
//    {
//        // TODO: add raw() method to Response.php
//        echo 'this will send a json response';
//
//        return $this;
//    }
//
//    public function &html(): ResponseHandler
//    {
//        // TODO: add html() method to Response.php
//        echo 'this will send a html response';
//
//
//
//
//
//
//        return $this;
//    }

    public function &json($content = [], int $status = 200): ResponseHandler
    {
        $this->response->format = ResponseFormat::JSON;
        $this->response->data = $content;
        $this->response->setStatusCode($status);

        // TODO: add json() method to Response.php
//        echo 'this will send a json response';
//        var_dump($content);


//        $this->response->getHeaderCollection()
//            ->add('Content-Type', 'application/json');



//        echo '<br />';
//        echo '<br />';
//
//        echo '<br />';
//        echo '<br />';
//        echo '<br />';
//        echo 'class Response';
//        echo '<pre>';
//        print_r($this->response->getHeaderCollection());
//        echo '/<pre>';



//        if ($this->response->hasHeader('content-type')) {
//            echo 'it has the header';
//        }


        return $this;
    }

    // public function &jsonp(): ResponseHandler
    // {
    //     $this->response->format = ResponseFormat::JSONP;
    //     // TODO: add jsonp() method to Response.php
    //     echo 'this will send a jsonp response';


    //     return $this;
    // }

//    public function &xml(): ResponseHandler
//    {
//        // TODO: add xml() method to Response.php
//        echo 'this will send a xml response';
//
//        return $this;
//    }
//
//    public function &text(): ResponseHandler
//    {
//        // TODO: add text() method to Response.php
//        echo 'this will send a text response';
//
//
//        return $this;
//    }

    public function ok(): bool
    {
        return (bool)$this->response->getStatusCode() === 200;
    }

}