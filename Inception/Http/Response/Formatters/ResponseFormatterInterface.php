<?php

namespace Snaju\Inception\Http\Response\Formatters;

use Snaju\Inception\Http\Response\Response;

interface ResponseFormatterInterface
{

    public function format(Response $response);

}