<?php

namespace Snaju\Inception\Http\Response\Formatters;

use Snaju\Inception\Http\Response\Response;

class HtmlResponseFormatter implements ResponseFormatterInterface
{

    /**
     * The Content-Type that will be sent in the response header.
     * @var string
     */
    public string $contentType = 'text/html';

    public function format(Response $response)
    {
        $response->getHeaderCollection()->add('Content-Type', $this->contentType);
    }

    public function send()
    {

    }

}