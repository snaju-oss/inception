<?php

namespace Snaju\Inception\Http\Response\Enums;

class ResponseFormat
{
    const RAW       = 'raw';
    const HTML      = 'html';
    const JSON      = 'json';
    const JSONP     = 'jsonp';
    const XML       = 'xml';
    const TEXT      = 'text';
}