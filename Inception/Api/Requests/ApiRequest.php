<?php

namespace Snaju\Inception\Api\Requests;

use Snaju\Inception\Api\ApiAuthResponse;
use Snaju\Inception\Api\Returns\ApiReturn;
use Snaju\Inception\Http\Request\WebRequest;
use Snaju\Inception\ORM\DataModel;

abstract class ApiRequest
{

    protected $method = "GET";

    protected ApiAuthResponse $auth;

    protected string $modelName;

    protected array $protected = [];

    protected abstract function process(WebRequest $request): ApiReturn;

    public function call(ApiAuthResponse $authResponse, string $modelName, WebRequest $request): ApiReturn
    {
        $this->auth = $authResponse;
        $this->modelName = $modelName;
        return $this->process($request);
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

}