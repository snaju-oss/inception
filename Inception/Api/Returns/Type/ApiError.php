<?php

namespace Snaju\Inception\Api\Returns\Type;

use Snaju\Inception\Api\Returns\ApiReturn;

class ApiError extends ApiReturn
{
    /**
     * ApiError constructor.
     * @param string $msg
     * @param int $errorCode
     */
    public function __construct(string $msg, int $errorCode = 500)
    {
        parent::__construct(false, $msg, [], $errorCode);
    }
}