<?php

namespace Snaju\Inception\Api\Returns\Type;

use Snaju\Inception\Api\Returns\ApiReturn;

class ApiSuccess extends ApiReturn
{
    /**
     * ApiSuccess constructor.
     */
    public function __construct($return = [])
    {
        parent::__construct(true, "", $return);
    }
}