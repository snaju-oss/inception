<?php

namespace Snaju\Inception\Api\Returns\Type;

use Snaju\Inception\Api\Returns\ApiReturn;

class ApiDataModel extends ApiReturn
{

    protected \Snaju\Inception\ORM\DataModel $model;

    /**
     * ApiModel constructor.
     * @param \Snaju\Inception\ORM\DataModel $model
     */
    public function __construct(\Snaju\Inception\ORM\DataModel $model)
    {
        $this->model = $model;
        $arr = $model->toArray()->get();

        parent::__construct(true, "", $arr);
    }
}