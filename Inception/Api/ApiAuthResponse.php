<?php

namespace Snaju\Inception\Api;

use Snaju\Inception\ORM\DataModel;

class ApiAuthResponse
{

    private $isAuthenticated = false;

    private ?DataModel $user;

    private array $scopes = [];

    /**
     * ApiAuthResponse constructor.
     * @param DataModel|null $user
     */
    public function __construct(?DataModel $user, array $scopes = [])
    {
        if ($user == null) {
            $this->isAuthenticated = false;
        } else {
            $this->isAuthenticated = true;
            $this->user = $user;
            $this->scopes = $scopes;
        }
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        return $this->isAuthenticated;
    }

    /**
     * @return DataModel|null
     */
    public function getModel(): ?DataModel
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }


}