<?php

namespace Snaju\Inception\Api;

use Snaju\Inception\Http\Request\WebRequest;

class ApiManager
{

    public static array $authProviders = [];
    public static ApiAuthProvider $currentProvider;
    public static ApiAuthResponse $auth;

    public static function registerProvider(ApiAuthProvider $apiAuthProvider)
    {
        self::$authProviders[] = $apiAuthProvider;
    }

    public static function processAuth(WebRequest $request): ApiAuthResponse
    {
        foreach (self::$authProviders as $provider) {
            if ($provider instanceof ApiAuthProvider) {
                $resp = $provider->auth($request);
                if($resp instanceof ApiAuthResponse) {
                    if($resp->isAuthenticated()) {
                        self::$currentProvider = $provider;
                        self::$auth = $resp;
                        return $resp;
                    }
                }
            }
        }

        return new ApiAuthResponse(null);
    }

}