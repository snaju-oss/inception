<?php

namespace Snaju\Inception\Api;

use Snaju\Inception\Http\Request\WebRequest;
use Snaju\Inception\ORM\DataModel;

interface ApiInterface
{

    public function api_get(WebRequest $request);

    public function api_post(WebRequest $request);

    public function api_list(WebRequest $request);

    public function api_patch(WebRequest $request);

    public function api_delete(WebRequest $request);

    public function hasAccess(DataModel $model, ApiAuthResponse $auth): bool;

}