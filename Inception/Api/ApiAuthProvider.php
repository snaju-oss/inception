<?php

namespace Snaju\Inception\Api;

use Snaju\Inception\Http\Request\WebRequest;
use Snaju\Inception\ORM\DataModel;

interface ApiAuthProvider
{
    /**
     * @param WebRequest $request
     * @return ApiAuthResponse
     * Process the auth
     */
    public function auth(WebRequest $request): ApiAuthResponse;

    /**
     * @param WebRequest $request
     * @return ApiAuthResponse
     * Return the current ApiAuth that is authenticated
     */
    public function getAuth(WebRequest $request): ApiAuthResponse;

    /**
     * @return DataModel
     * Return the related DataModel for the auth
     */
    public function getCurrentModel(): DataModel;
}