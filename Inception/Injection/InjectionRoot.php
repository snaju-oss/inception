<?php

namespace Snaju\Inception\Injection;

use Doctrine\Common\Annotations\AnnotationReader;
use Snaju\Inception\Annotation\Inject;

abstract class InjectionRoot
{
    public function __construct()
    {
        InjectionManager::inject($this);
    }
}