<?php

namespace Snaju\Inception\Route;

class RouteFile
{
    private $func;

    /**
     * RouteFile constructor.
     * @param $func
     */
    public function __construct($func)
    {
        $this->func = $func;
    }

    /**
     * @return mixed
     */
    public function getFunc()
    {
        return $this->func;
    }

    /**
     * @param mixed $func
     */
    public function setFunc($func): void
    {
        $this->func = $func;
    }

}