<?php

namespace Snaju\Inception\Route;

use Snaju\Inception\Http\Request\WebRequest;
use Snaju\Inception\InceptionCore;

class CustomRoute extends Route
{

    protected \Closure $run;

    /**
     * CustomRoute constructor.
     * @param \Closure $run
     */
    public function __construct(\Closure $run, string $path)
    {
        $this->run = $run;

        parent::__construct($path);
    }

    public function onCall(WebRequest $request)
    {
        $this->run->call($this, InceptionCore::getRequest());
    }
}