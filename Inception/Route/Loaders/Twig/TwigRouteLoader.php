<?php

namespace Snaju\Inception\Route\Loaders\Twig;

use Snaju\Inception\Route\RouteFile;
use Snaju\Inception\Settings\Config;
use Snaju\Inception\InceptionCore;
use Snaju\Inception\Route\RouteLoader;
use Snaju\Inception\Route\RouteManager;
use Snaju\Inception\Util\Loader;
use Twig\Environment;

class TwigRouteLoader implements RouteLoader
{

    private ?string $templateDir;

    /**
     * TwigRouteLoader constructor.
     * @param ?string $templateDir
     */
    public function __construct(string $templateDir = null)
    {
        if (!empty($templateDir) || $templateDir !== null) {
            $this->templateDir = $templateDir;
        } else {
            $this->templateDir = config('app.viewsDirectory');
        }
    }

    public function loadRoutes()
    {
        // Initialize "default" scope for data loading
        // Make the template manager and set the Environment for Twig Loading

        $env = new Environment(new TemplateLoader());
//        $env->setLexer(new TwigLexer($env));

        TemplateManager::setTwigEnv($env);
        TemplateManager::$context = [];
//        TemplateManager::$env = new Environment(new TemplateLoader());

//        TemplateManager::addData();


        // Load all the templates from the specified template directory
        $templateDirectory = Config::get('app.templatesDirectory');

        $loader = new Loader(InceptionCore::getBase() . "/" . Config::get('app.templatesDirectory'), '/.*\.(twig|html)/mix', false);


//        echo "<pre>";
//        print_r($loader);
//        echo "</pre>";


        // TODO: clean up both loops to make one and determine whether to regsiter as route based on if it has 'views' in the file name


        foreach ($loader->getMatches() as $matchLoc) {
            preg_match('/(.*)\.(twig|html)/mi', $matchLoc, $m);

            // remove the base from the file system.
            $relative = str_replace(array(InceptionCore::getBase() . "/" . $templateDirectory), "", $m[1]);
            $relative = ltrim($relative, '/');

//            echo "this is a route::";
//            echo $relative;
//
//            echo "<pre>";
//            print_r($matchLoc);
//            echo "</pre>";


            // Make the template object for TwigLoader
            $template = new Template($relative, $matchLoc);

            // Register the template with the Template Manager
            TemplateManager::register($template);
        }


        // Find all .twig/.html files in the template dir.
        $loader = new Loader(InceptionCore::getBase() . "/" . $this->templateDir, '/.*\.(twig|html)/mix', false);


//        echo "<pre>";
//        print_r($loader);
//        echo "</pre>";


        foreach ($loader->getMatches() as $matchLoc) {

            preg_match('/(.*)\.(twig|html)/mi', $matchLoc, $m);


//            echo "<pre>";
//            print_r($matchLoc);
//            echo "</pre>";


            // remove the base from the file system.
            $relative = str_replace(array(InceptionCore::getBase() . "/" . $this->templateDir, "index"), "", $m[1]);
            $urlRelative = $relative;

            $domain = null;
            if (preg_match('/\@(.*?)(\/.*)/mi', $relative, $domainLimit)) {
                // Has a domain in the list.
                $domain = $domainLimit[1];
                $urlRelative = str_replace("/@$domain", "", $urlRelative);
            }

            // Make the template object for TwigLoader
            $template = new Template($relative, $matchLoc);

            // Register the template with the Template Manager
            TemplateManager::register($template);

            // Make the new route
            $route = new TwigRoute($urlRelative, $relative);

            // Make the route and register it
            RouteManager::register($route, $domain);
        }
    }
}