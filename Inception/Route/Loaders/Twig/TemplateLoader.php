<?php

namespace Snaju\Inception\Route\Loaders\Twig;

use Snaju\Inception\InceptionCore;
use Snaju\Inception\Util\Loaders\Queue;
use Snaju\Inception\Util\Loaders\Stack;
use Snaju\Inception\Util\Util;
use Twig\Loader\FilesystemLoader;
use Twig\Source;

class TemplateLoader implements \Twig\Loader\LoaderInterface
{

    /**
     * Returns the source context for a given template logical name.
     *
     * @param string $name
     * @return \Twig\Source
     * @throws \Twig\Error\LoaderError When $name is not found
     */
    public function getSourceContext(string $name): \Twig\Source
    {
        if (TemplateManager::exist($name)) {
            $t = TemplateManager::$templates[$name];
            if ($t instanceof Template) {
                if (file_exists($t->getLocation())) {
                    /*
                     * Return the Source
                     * */
//
//                    $var = $t->getLocation();
//                    $varDir = dirname($var);
//                    $dirs = str_replace(InceptionCore::getBase(), "", $varDir);
//                    $dParts = Util::cleanArray(explode("/", $dirs));
//                    $i = count($dParts);
//                    foreach ($dParts as $ii => $part) {
//                        $allname = InceptionCore::getBase() . "/" . implode("/", $dParts) . "/" . "_all.php";
//                        if (file_exists($allname)) {
//                            TemplateManager::$controllers[] = $allname;
//                        }
//                        unset($dParts[--$i]);
//                    }
//
//                    $phpLoc = str_replace(".twig", ".php", $var);
//
//                    if (file_exists($phpLoc)) {
//                        TemplateManager::$controllers[] = $phpLoc;
//                    }

                    return new Source(file_get_contents($t->getLocation()), $t->getId(), '');
                } else {
                    throw new \Exception("Template Source File Not Found");
                }
            } else {
                throw new \Exception("Template invalid type");
            }
        } else {
            throw new \Exception("Template Not Found by ID [" . $name . "]");
        }

        throw new \Twig\Error\LoaderError();
    }

    /**
     * Gets the cache key to use for the cache for a given template name.
     *
     * @throws \Twig\Error\LoaderError When $name is not found
     */
    public function getCacheKey(string $name): string
    {
        return $name;
    }

    /**
     * @param int $time Timestamp of the last modification time of the cached template
     *
     * @throws \Twig\Error\LoaderError When $name is not found
     */
    public function isFresh(string $name, int $time): bool
    {
        return true;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function exists(string $name)
    {
        if (TemplateManager::exist($name)) {
            $t = TemplateManager::$templates[$name];
            if ($t instanceof Template) {
                if (file_exists($t->getLocation())) {
                    return true;
                }
            }
        }

        return false;
    }
}

?>