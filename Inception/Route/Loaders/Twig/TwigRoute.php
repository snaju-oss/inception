<?php

namespace Snaju\Inception\Route\Loaders\Twig;

use Snaju\Inception\Exception\Exception;
use Snaju\Inception\Http\Request\WebRequest;
use Snaju\Inception\InceptionCore;
use Snaju\Inception\Route\Route;
use Snaju\Inception\Util\Loaders\Queue;
use Snaju\Inception\Util\Util;
use Twig\Compiler;
use Twig\Environment;
use Twig\Extension\CoreExtension;
use Twig\Loader\ArrayLoader;
use Twig\Node\IncludeNode;
use Twig\Node\Node;
use Twig\Source;
use Twig\TemplateWrapper;

class TwigRoute extends Route
{

    private string $twigId;

    private Queue $allExecuted;

    private array $controllers = [];

    private Source $source;

    private Environment $env;

    /**
     * TwigRoute constructor.
     * @param $pathTo
     * @param $twigId
     */
    public function __construct($pathTo, $twigId)
    {
        $this->twigId = $twigId;
        $this->allExecuted = new Queue();

        parent::__construct($pathTo);
    }

    public function preLoadTemplate($twigId)
    {
        if (isset(TemplateManager::$templates[$twigId])) {
            $template = TemplateManager::$templates[$twigId];
            if ($template instanceof Template) {
                $this->source = new Source(file_get_contents($template->getLocation()), $template->getId(), '');
                $stream = TemplateManager::$env->tokenize($this->source);
                $parse = TemplateManager::$env->parse($stream);

                $this->searchForControllers($parse);
            }
        }
    }

    public function fromIdToControllers($twigId, bool $pushFirst = false): void
    {
        if ($twigId != "" && !empty($twigId)) {
            if (isset(TemplateManager::$templates[$twigId])) {
                $t = TemplateManager::$templates[$twigId];
                if ($t instanceof Template) {
                    $var = $t->getLocation();
                    $varDir = dirname($var);
                    $dirs = str_replace(InceptionCore::getBase(), "", $varDir);
                    $dParts = Util::cleanArray(explode("/", $dirs));
                    $i = count($dParts);
                    foreach ($dParts as $ii => $part) {
                        $allname = InceptionCore::getBase() . "/" . implode("/", $dParts) . "/" . "_all.php";
                        if (file_exists($allname)) {
                            if ($pushFirst) {
                                array_unshift($this->controllers, $allname);
                            } else {
                                $this->controllers[] = $allname;
                            }
                        }
                        unset($dParts[--$i]);
                    }

                    $phpLoc = str_replace(".twig", ".php", $var);

                    if (file_exists($phpLoc)) {
                        if ($pushFirst) {
                            array_unshift($this->controllers, $phpLoc);
                        } else {
                            $this->controllers[] = $phpLoc;
                        }
                    }
                } else {
                    echo("Invalid Template Object by ID " . $twigId);
                }
            } else {
                echo("No template with ID " . $twigId . " found in system");
            }
        }
    }

    public function searchForControllers(Node $node)
    {
        if ($node->hasNode("parent")) {
            $this->preLoadTemplate($node->getNode("parent")->getAttribute("value"));
            $this->fromIdToControllers($node->getNode("parent")->getAttribute("value"), true);
        }

        if ($node instanceof IncludeNode) {
            if ($node->hasNode("expr")) {

                $str = TemplateManager::$env->compile($node->getNode("expr"));
                $context = (array)TemplateManager::$context;

                ob_start();
                eval("use Twig\Extension\CoreExtension;" .
                    "echo " . $str . ";");
                $realVal = ob_get_clean();

                $this->preLoadTemplate($realVal);
                $this->fromIdToControllers($realVal);

//                $parentCode = $node->getSourceContext()->getCode();
//                $lineNumber = $node->getTemplateLine();
//
//                $lines = explode("\n", $parentCode);
//
//                $line = $lines[$lineNumber - 1];
//
//                $realVal = "";
//                if (preg_match('/\{\%\s*include\s*(.*?)\s*\%\}/mi', $line, $m)) {
//                    $realVal = $m[1];
//                }
//
//                $realVal = TemplateManager::renderString("{{" . $realVal . "}}", TemplateManager::getAll());

//                $str = TemplateManager::$env->compile($node->getNode("expr"));
//                $context = (array)TemplateManager::$context;
//
//                /*
//                 * Generate file to execute the Twig Functions
//                 * */
//                $tmpFile = tmpfile();
//                fwrite($tmpFile, "<?php " .
                /*                    " return " . $str . "; ?>");*/
//
//                $tmpFilePath = stream_get_meta_data($tmpFile)['uri'];
//
//                $realVal = include $tmpFilePath;
//
//                fclose($tmpFile);

//                ob_start();
//                TemplateManager::$env->display($str, TemplateManager::getAll());
//                $abc = ob_get_clean();

//                echo $realVal;
//                echo "<hr/>";

//                $this->preLoadTemplate($realVal);
//                $this->fromIdToControllers($realVal);
            }
        }

        foreach ($node->getIterator() as $n) {
            if ($n instanceof Node) {
                $this->searchForControllers($n);
            }
        }

    }

    public function runControllers()
    {
        foreach ($this->controllers as $var) {
            $a1 = require $var;
            if (is_array($a1)) {
                TemplateManager::merge($a1);
            }
        }
    }

    public function getTwigId(): string
    {
        return $this->twigId;
    }

    public function getAllExecuted(): Queue
    {
        return $this->allExecuted;
    }

    public function getControllers(): array
    {
        return $this->controllers;
    }

    public function getSource(): Source
    {
        return $this->source;
    }

    public function getEnv(): Environment
    {
        return $this->env;
    }


    public function onCall(WebRequest $request)
    {
        $this->env = TemplateManager::$env;
        $this->fromIdToControllers($this->twigId);
        $this->preLoadTemplate($this->twigId);
        $this->runControllers();

        // Final Render Pass
        $render = TemplateManager::render($this->twigId);

        echo $render;
//        exit(200);
    }
}