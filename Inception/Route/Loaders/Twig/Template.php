<?php

namespace Snaju\Inception\Route\Loaders\Twig;

class Template
{

    private $id;

    private $location;

    /**
     * Template constructor.
     * @param $id
     * @param $location
     */
    public function __construct($id, $location)
    {
        $this->location = $location;
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location): void
    {
        $this->location = $location;
    }

}

?>