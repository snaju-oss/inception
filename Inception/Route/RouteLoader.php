<?php

namespace Snaju\Inception\Route;

interface RouteLoader
{
    public function loadRoutes();
}