<?php

namespace Snaju\Inception\Route;

use Snaju\Inception\Http\Request\WebRequest;

abstract class Route
{
    public array $data = [];

    private array $files = [];

    private $fullPath;

    public abstract function onCall(WebRequest $request);


    public function __construct($fullPath)
    {
        $this->fullPath = $fullPath;
    }

    /**
     * @return mixed
     */
    public function getFullPath()
    {
        return $this->fullPath;
    }

    /**
     * @param mixed $fullPath
     */
    public function setFullPath($fullPath): void
    {
        $this->fullPath = $fullPath;
    }

    public function attach(RouteFile $file)
    {
        $this->files[] = $file;
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        return $this->files;
    }

}