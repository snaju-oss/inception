<?php

namespace Snaju\Inception\ORM;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class MagicIdType extends Type
{

    const MAGIC_ID = "magicid";

    public function getSQLDeclaration(array $column, AbstractPlatform $platform)
    {
        return "TEXT";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value == "none") {
            return null;
        }

        return ORM::getDataModelFromMagicId($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DataModel) {
            return $value->magicId();
        } else if (is_string($value)) {
            return $value;
        }

        return "none";
    }

    public function getName()
    {
        return self::MAGIC_ID;
    }
}
