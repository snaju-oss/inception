<?php

namespace Snaju\Inception\ORM;

use Snaju\Inception\Util\Loaders\Stack;

interface DataModelProcessor
{

    public function onToArray(array &$array, Stack &$stack = null);

    public function preOnArray();

}