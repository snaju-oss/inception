<?php

namespace Snaju\Inception\Util;

// TODO: add docs
class ClassAsyncLoader
{

    public static $classes = [];

    public static function fromFile($fileName)
    {
        $content = file_get_contents($fileName);

        $classes = [];

        $namespace = "";
        if (preg_match('/^namespace\s*(.*)\;/mix', $content, $m1)) {
            $namespace = $m1[1];
        }

        if (preg_match_all('/^class\s*(.*?)\s*(extends|impliments).*\n/mix', $content, $m, PREG_SET_ORDER, 0)) {
            // We have a match

            foreach ($m as $p) {
                $r = new \ReflectionClass("\\" . $namespace . "\\" . $p[1]);
                $classes[] = $r;
                self::$classes["\\" . $namespace . "\\" . $p[1]] = $r;
            }
        }

        return $classes;
    }

}