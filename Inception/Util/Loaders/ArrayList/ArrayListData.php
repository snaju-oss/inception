<?php

namespace Snaju\Inception\Util\Loaders\ArrayList;

class ArrayListData
{

    private ArrayListData|null $previous;

    private ArrayListData|null $next;

    private ?string $key;

    private $data;

    /**
     * ArrayListData constructor.
     * @param $previous
     * @param string $key
     * @param $data
     */
    public function __construct($previous, $next, ?string $key, $data)
    {
        $this->previous = $previous;
        $this->next = $next;
        $this->key = $key;
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param mixed $next
     */
    public function setNext($next): void
    {
        $this->next = $next;
    }

    /**
     * @return mixed
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * @param mixed $previous
     */
    public function setPrevious($previous): void
    {
        $this->previous = $previous;
    }

    /**
     * @return string
     */
    public function getKey(): string|null
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

}

?>