<?php

namespace Snaju\Inception\Util\Loaders;

class Queue
{

    protected array $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function isEmpty(): bool
    {
        if ($this->count() >= 1) {
            return false;
        }
        return true;
    }

    public function count(): int
    {
        return count($this->data);
    }

    public function toArray(): array
    {
        return (array)$this->data;
    }

    public function enqueue($object): void
    {
        $this->data[] = $object;
    }

    /**
     * Retrieves the next item in the stack without removing it.
     * @return mixed|null
     */
    public function peek()
    {
        if ($this->isEmpty()) {
            return null;
        }
        return $this->data[0];
    }

    /**
     * Retrieves the next item in the stack.
     * @return mixed|null
     */
    public function dequeue()
    {
        if ($this->isEmpty()) {
            return null;
        }
        return array_shift($this->data);
    }

    /**
     * Removes all the items from the queue and rests it.
     */
    public function clear(): void
    {
        $this->data = [];
    }

    /**
     * Alias of clear method.
     */
    public function empty(): void
    {
        $this->clear();
    }

    public function contains($obj): bool
    {
        foreach ($this->data as $o) {
            if ($o == $obj) {
                return true;
            }
        }

        return false;
    }

}