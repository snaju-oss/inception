<?php

namespace Snaju\Inception\Util;

abstract class TwigObject implements \ArrayAccess
{

    private $__meta = [];

    public function offsetExists(mixed $offset)
    {
        return (
            isset($this->getVars()[$offset]) ||
            isset($this->__meta[$offset]) || $this->magicMethodExist($offset)
        );
    }

    public function offsetGet(mixed $offset)
    {
        if (isset($this->getVars()[$offset])) {
            if (is_resource($this->getVars()[$offset])) {
                return stream_get_contents($this->getVars()[$offset]);
            }
            return $this->getVars()[$offset];
        }

        if (isset($this->__meta[$offset])) {
            if (is_resource($this->__meta[$offset])) {
                return stream_get_contents($this->__meta[$offset]);
            }
            return $this->__meta[$offset];
        }

        if ($this->magicMethodExist($offset)) {
            return $this->callMagicFunction($offset);
        }

        return null;
    }

    public function offsetSet($offset, $value): void
    {
        $this->__meta[$offset] = $value;
    }

    public function offsetUnset(mixed $offset)
    {
        unset($this->__meta[$offset]);
    }

    public function getVars()
    {
        return get_object_vars($this);
    }

    public function __call(string $name, array $arguments)
    {
        return $this->{$name};
    }

    public function __get($name)
    {
        if (isset($this->__meta[$name])) {
            if (is_resource($this->__meta[$name])) {
                return stream_get_contents($this->__meta[$name]);
            }

            return $this->__meta[$name];
        }

        if ($this->magicMethodExist($name)) {
            return $this->callMagicFunction($name);
        }

        return null;
    }

    private function callMagicFunction(string $name)
    {
        $c = new \ReflectionClass(self::getClassName());
        if ($c instanceof \ReflectionClass) {
            if ($c->hasMethod($name)) {
                $m = $c->getMethod($name);
                if ($m instanceof \ReflectionMethod) {
                    if ($m->isInternal() && $m->getNumberOfRequiredParameters() == 0) {
                        return $m->invoke($this);
                    }
                }
            }
        }
        return null;
    }

    public function __set($name, $value)
    {
        $this->__meta[$name] = $value;
    }

    public function __isset($name)
    {
        if (array_key_exists($name, $this->getVars())) {
            return true;
        }

        if (isset($this->__meta[$name])) {
            return true;
        }

        if ($this->magicMethodExist($name)) {
            return true;
        }

        return false;
    }

    public function __toString()
    {
        return "InceptionDataModel." . self::getUUIDString();
    }

    private function magicMethodExist(string $name)
    {
        $c = new \ReflectionClass(self::getClassName());
        if ($c instanceof \ReflectionClass) {
            if ($c->hasMethod($name)) {
                $m = $c->getMethod($name);
                if ($m->isInternal() && $m instanceof \ReflectionMethod) {
                    if ($m->getNumberOfRequiredParameters() == 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public
    static function getClassName()
    {
        return get_called_class();
    }

}