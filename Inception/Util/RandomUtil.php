<?php

namespace Snaju\Inception\Util;

class RandomUtil
{

    public static function randomNumber(int $amt = 1)
    {
        $str = "";
        for ($i = 0; $i < $amt; $i++) {
            $str .= rand(0, 9);
        }

        return $str;
    }

}

?>