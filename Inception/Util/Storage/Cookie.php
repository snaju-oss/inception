<?php

namespace Snaju\Inception\Util\Storage;

/**
 * Class Cookie
 * Object-oriented convenience wrapper class for handling cookies.
 *
 * @package Snaju\Inception\Util\Storage
 */
class Cookie
{
    /**
     * Checks if a cookie exists.
     *
     * @param $name
     * @return bool
     */
    public static function exists($name): bool
    {
        return isset($_COOKIE[$name]);
    }

    /**
     * Retrieves the delicious cookie value or returns an empty string.
     *
     * @param $name
     * @return string
     */
    public static function get($name): string
    {
        if (self::exists($name)) {
            return $_COOKIE[$name];
        }

        return '';
    }

    /**
     * Sets a cookie.
     *
     * @param mixed ...$args
     */
    public static function set(...$args): void
    {
        setcookie(...$args);
    }

    /**
     * Removes a cookie
     *
     * @param string $name
     */
    public static function remove(string $name): void
    {
        setcookie($name, '', time() - 3600);
    }

    /**
     * Retrieves the raw cookie array.
     *
     * @return array
     */
    public function getRaw(): array
    {
        return $_COOKIE;
    }

}