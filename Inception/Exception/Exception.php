<?php

namespace Snaju\Inception\Exception;

class Exception extends \Exception {

    /**
     * Returns a human-readable/friendly name for this exception.
     *
     * @return string
     */
    public function getName(): string
    {
        return (string)get_class($this);
    }

}