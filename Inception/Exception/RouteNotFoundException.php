<?php

namespace Snaju\Inception\Exception;

class RouteNotFoundException extends InvalidArgumentException
{
    public function getName(): string
    {
        return 'Route Not Found';
    }
}