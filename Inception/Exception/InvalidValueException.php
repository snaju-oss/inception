<?php

namespace Snaju\Inception\Exception;

class InvalidValueException extends \UnexpectedValueException
{
    public function getName(): string
    {
        return 'Invalid Return Value';
    }
}