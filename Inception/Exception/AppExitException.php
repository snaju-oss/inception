<?php

namespace Snaju\Inception\Exception;

use Throwable;


/**
 * Exception not meant to be catched, but used to gracefully terminate
 * the application.
 *
 * @package Snaju\Inception\Exception
 */
class AppExitException extends Exception
{

//    public int $statusCode;

//    public function __construct($status, $message = "", $code = 0, Throwable $previous = null)
//    {
//        $this->statusCode = $status;
//        parent::__construct($message, $code, $previous);
//    }

    public function getName(): string
    {
        return 'App Exit';
    }

}