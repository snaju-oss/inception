<?php

namespace Snaju\Inception\Exception;

class InvalidConfigException extends Exception
{
    public function getName(): string
    {
        return 'Invalid Configuration';
    }
}