<?php

namespace Snaju\Inception\Exception;

class InvalidKeyException extends InvalidParamException
{
    public function getName(): string
    {
        return 'Invalid Key';
    }
}