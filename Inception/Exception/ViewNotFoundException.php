<?php

namespace Snaju\Inception\Exception;

class ViewNotFoundException extends InvalidArgumentException
{
    public function getName(): string
    {
        return 'View Not Found';
    }
}