<?php

namespace Snaju\Inception\Exception;

use Throwable;

class PluginNotInstalledException extends Exception
{
    public function __construct($pluginName, $from)
    {
        parent::__construct("The plugin {$pluginName} is not installed and is required by {$from}", -1, null);
    }

}