<?php

namespace Snaju\Inception\Exception;

class UnsupportedMethodException extends \BadMethodCallException
{
    public function getName(): string
    {
        return 'Unsupported Method';
    }
}