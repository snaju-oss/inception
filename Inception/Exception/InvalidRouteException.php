<?php

namespace Snaju\Inception\Exception;

class InvalidRouteException extends UserDisplayException
{
    public function getName(): string
    {
        return 'Invalid Route';
    }
}