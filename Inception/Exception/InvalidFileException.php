<?php

namespace Snaju\Inception\Exception;

class InvalidFileException extends Exception
{
    public function getName(): string
    {
        return 'Invalid File';
    }
}