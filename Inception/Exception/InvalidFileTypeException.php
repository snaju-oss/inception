<?php

namespace Snaju\Inception\Exception;

class InvalidFileTypeException extends Exception
{
    public function getName(): string
    {
        return 'Invalid File Type';
    }
}