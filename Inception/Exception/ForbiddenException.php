<?php

namespace Snaju\Inception\Exception;

class ForbiddenException extends UserDisplayException
{
    public function getName(): string
    {
        return 'Forbidden';
    }
}