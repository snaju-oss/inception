<?php

namespace Snaju\Inception\Exception;

/**
 * Exceptions meant to be shown the end user whether it be an error on a
 * Twig template or an error response message directly from the backend via API.
 *
 * @package Snaju\Inception\Exception
 */
class UserDisplayException extends Exception
{
//    public function getName(): string
//    {
//        return 'User Exception';
//    }
}