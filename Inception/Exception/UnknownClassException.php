<?php

namespace Snaju\Inception\Exception;

class UnknownClassException extends Exception
{
    public function getName(): string
    {
        return 'Unknown Class';
    }
}