<?php

namespace Snaju\Inception\Exception;

class InvalidArgumentException extends InvalidParamException
{
    public function getName(): string
    {
        return 'Invalid Argument';
    }
}