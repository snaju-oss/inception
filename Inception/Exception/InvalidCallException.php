<?php

namespace Snaju\Inception\Exception;

class InvalidCallException extends \BadMethodCallException
{
    public function getName(): string
    {
        return 'Invalid Call';
    }
}