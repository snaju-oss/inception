<?php

namespace Snaju\Inception\Exception;

class InvalidParamException extends \BadMethodCallException
{
    public function getName(): string
    {
        return 'Invalid Parameter';
    }
}