<?php

namespace Snaju\Inception\Exception;

class DefaultUncaughtException extends Exception
{
    public function getName(): string
    {
        return 'Uncaught Exception';
    }
}