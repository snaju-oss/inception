<?php

namespace Snaju\Inception\Exception;

class UnknownMethodException extends \BadMethodCallException
{
    public function getName(): string
    {
        return 'Unknown Method';
    }
}