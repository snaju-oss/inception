<?php

namespace Snaju\Inception\Exception;

class ModelNotFoundException extends InvalidArgumentException
{
    public function getName(): string
    {
        return 'Model Not Found';
    }
}