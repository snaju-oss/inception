<?php

namespace Snaju\Inception\Exception;

class UnknownPropertyException extends Exception
{
    public function getName(): string
    {
        return 'Unknown Property';
    }
}