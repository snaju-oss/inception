<?php

namespace Snaju\Inception\Exception;

/**
 * @see ModelNotFoundException
 * @deprecated
 * @package Snaju\Inception\Exception
 */
class NoModelFoundException extends UserDisplayException
{
    public function getName(): string
    {
        return 'No Model Found';
    }
}