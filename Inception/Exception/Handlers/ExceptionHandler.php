<?php

namespace Snaju\Inception\Exception\Handlers;

use Snaju\Inception\Event\Types\OnExceptionEvent;
use Snaju\Inception\Http\Request\WebRequest;
use Snaju\Inception\Util\Loaders\Collection;
use Exception;

class ExceptionHandler
{
    public function __invoke($exception)
    {
        if ($exception instanceof Exception) {
            (new OnExceptionEvent($exception))->call();
        }
    }
}