<?php

namespace Snaju\Inception\Exception\Handlers;

use Exception;
use Snaju\Inception\Http\Request\WebRequest;
use Snaju\Inception\InceptionCore;

abstract class AbstractErrorHandler
{
    protected WebRequest $request;
    protected Exception $exception;
    protected string $exceptionName;
    protected bool $devMode;
    protected bool $debugMode;
    public int $priority;

    /**
     * Determines if the handler should be considered in the final error resolution
     * to return the error in the designated format.
     * @return bool
     */
    abstract public function shouldRender(): bool;

    /**
     * Handles rendering of the error based on the format this class is configured for.
     * @param Exception $exception
     * @return mixed
     */
    abstract public function handle(Exception $exception);

    /**
     * AbstractErrorHandler constructor.
     */
    public function __construct()
    {
        $this->request = InceptionCore::getRequest();
        $this->devMode = $_ENV['APP_ENV'] !== 'production';
        $this->debugMode = $_ENV['APP_DEBUG'] == 'true' ? true : false;
    }

    /**
     * Sets the ExceptionHandler's name
     *
     * @param Exception $exception
     */
    protected function setExceptionName(Exception $exception): void
    {
        $this->exceptionName =(string)preg_replace('/.*\\\\/', '', get_class($exception));
    }

    /**
     * Retrieves the name of the ExceptionHandler.
     * @return string
     */
    public function getExceptionName(): string
    {
        return $this->exceptionName;
    }
    
}