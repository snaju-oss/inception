<?php

namespace Snaju\Inception\Exception;

class NotSupportedException extends Exception
{
    public function getName(): string
    {
        return 'Not Supported';
    }
}