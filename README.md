
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->



<!-- PROJECT LOGO -->
<br />

<div style="display:inline-block;padding:8px 8px 0 8px;background-color:#132F48">
    <a href="https://github.com/snaju-oss/inception">
        <img src="https://snaju.com/assets/img/logo.svg" alt="Logo" width="auto" height="80">
    </a>
</div>

<h3>inception</h3>

  <p>
    A lightweight PHP based web framework built by Snaju Development
    <br />
    <a href="https://docs.dev.snaju.com/inception"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://git.hou.snaju.com/snaju-oss/inception/issues">Report Bug</a>
    ·
    <a href="https://git.hou.snaju.com/snaju-oss/inception/issues">Request Feature</a>
  </p>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <!--<li><a href="#license">License</a></li>-->
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

1. PHP `^7.4|^8.0`
2. NodeJS `^14.16`

### Installation

1. Clone the repo
   ```sh
   git clone https://git.hou.snaju.com/snaju-oss/inception-framework.git
   ```
3. Install node dependencies
   ```sh
   npm install
   ```
4. Install composer dependencies
   ```JS
   composer install
   ```
5. Move `.env-example` to `.env` and create database and update details
6. If using port for php server other than `8888`, update `gulpfile.js` line 29 with the proper port

<!-- USAGE EXAMPLES -->
## Usage

* For dev you can use one of the npm scripts. `npm run start` will start a dev server that injects js/css for live reloading and refreshes when templates are updated. Note: If using an IDE like PHPStorm, updates to file system are delayed which means the reload is not triggered until the IDE is unfocused.

* If using port for php server other than `8888`, update `gulpfile.js` line 29 with the proper port

// TODO

_For more examples, please refer to the [Documentation](https://example.com)_



<!-- ROADMAP -->
## Roadmap

- [X] File-based Routing System
- [X] Twig Rendering
- [X] Global & Route Middleware
- [X] Route Events (Start, Process, End, Fallback)
- [X] Basic Response Handling (view, json)
- [X] Request Validators
- [X] Exception Handling
- [X] Global Functions
- [] Plugins
- [] Auto API Adapters and Responses


See the [open issues](https://git.hou.snaju.com/snaju-oss/inception-framework/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Snaju Development - [@snajullc](https://twitter.com/snajullc) - dev@snaju.com

Project Link: [https://git.hou.snaju.com/snaju-oss/inception](https://git.hou.snaju.com/snaju-oss/inception)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [PHP dotenv - .env file reader](https://github.com/vlucas/phpdotenv)
* [Doctrine ORM Quick Reference](http://ormcheatsheet.com)
* [phpDocumentor - Generate docs from code](https://www.phpdoc.org/)